<?php
namespace PhinxHelper\Helpers;

class ConfigGenerator
{
    protected $config = array(
        'adapter'   => 'mysql',
        'host'      => 'localhost',
        'name'      => 'production_db',
        'user'      => 'root',
        'pass'      => '',
        'port'      => '3306',
        'charset'   => 'utf8',
        'collation' => 'utf8_general_ci',
    );

    protected $configSource = array();

    /**
     * Minimal form of config array is
     * <pre>
     * $config = array(
     *      'database' => array(
     *          'dns' => 'mysql:host=127.0.0.1;dbname=test;',
     *          'username' => 'test',
     *          'password' => '',
     *      ),
     *      'migrations_path' => '../../migrations'
     * );
     * </pre>
     *
     * <i>Note: Migration path is relative path to phinx.php file</i>
     *
     * @param array $config database connection settings
     */
    public function __construct(array $config)
    {
        if (!isset($config['database']) || !isset($config['migrations_path'])) {
            throw new \InvalidArgumentException('Config is in incorrect format, please check documentation.');
        }
        $this->configSource = $config;
        $this->parseConfig();
    }

    protected function parseConfig()
    {
        $config = $this->configSource;

        $parts = explode(';', $config['database']['dsn']);
        foreach ($parts as $part) {
            list($key, $value) = explode('=', $part);
            if (strpos($key, ':') !== false) {
                list($adapter, ) = explode(':', $key);
                $this->config['adapter'] = $adapter;
                $this->config['host'] = $value;
                continue;
            } elseif ($key === 'dbname') {
                $this->config['name'] = $value;
                continue;
            } else {
                $this->config[$key] = $value;
            }
        }

        $this->config['user'] = $config['database']['user'];
        $this->config['pass'] = $config['database']['password'];
    }

    public function getConfig()
    {
        return array(
            "paths" => array(
                "migrations" => $this->configSource['migrations_path'],
            ),
            "environments" => array(
                "default_migration_table" => "phinxlog",
                "default_database" => "development",
                "development" => $this->config,
            )
        );
    }
}
