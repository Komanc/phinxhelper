<?php

namespace PhinxHelper\Helpers;


class Options {
    /**
     * set maximum length for strings
     */
    const LIMIT_KEY = 'limit';
    /**
     * alias for limit
     */
    const LENGTH_KEY = 'limit';

    /**
     * set default value or action
     */
    const DEFAULT_KEY = 'default';

    /**
     * allow NULL values
     */
    const NULL_KEY = 'null';

    /**
     * possible values for SET or ENUM columns (only applies to MySQL)
     */
    const VALUES_KEY = 'values';

    /**
     * combine with scale set to set decimal or float accuracy
     *
     * set count of all numbers in number (must be &gt;= {@see Options::SCALE_KEY})
     */
    const PRECISION_KEY = 'precision';

    /**
     * combine with precision to set decimal or float accuracy
     *
     * set count of decimal numbers in number (must be &lt;= {@see Options::PRECISION_KEY})
     */
    const SCALE_KEY = 'scale';

    /**
     * specify the column that a new column should be placed after
     */
    const AFTER_KEY = 'after';

    /**
     * set an action to be triggered when the row is updated
     */
    const UPDATE_KEY = 'update';

    /**
     * set an action to be triggered when the row is deleted
     */
    const DELETE_KEY = 'delete';

    /**
     * set a text comment on the column
     */
    const COMMENT_KEY = 'comment';

    /**
     * enable or disable the UNSIGNED option (only applies to MySQL)
     */
    const SIGNED_KEY = 'signed';


    const TYPE_KEY = 'type';
    const UNIQUE_KEY = 'unique';
    const NAME_KEY = 'name';

    const ON_ACTION_SET_NULL = 'SET_NULL';
    const ON_ACTION_NO_ACTION = 'NO_ACTION';
    const ON_ACTION_CASCADE = 'CASCADE';
    const ON_ACTION_RESTRICT = 'RESTRICT';
}