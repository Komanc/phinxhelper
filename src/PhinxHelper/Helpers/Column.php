<?php

namespace PhinxHelper\Helpers;

class Column
{
    const STRING = 'string';
    const TEXT   = 'text';

    const INT     = 'integer';
    const BIGINT  = 'biginteger';
    const FLOAT   = 'float';
    const DECIMAL = 'decimal';

    const DATETIME  = 'datetime';
    const TIMESTAMP = 'timestamp';
    const TIME      = 'time';
    const DATE      = 'date';

    const BINARY  = 'binary';
    const BOOLEAN = 'boolean';
    const JSON    = 'json';

    const ENUM = 'enum';
    const SET  = 'set';
}