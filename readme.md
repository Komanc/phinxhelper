PhinxHelper
=============

Provides two class with few constants which holds column types and column options.

In your migration you can use


```
#!php

C::BIGINT      // C is alias for Column
O::DEFAULT_KEY // O is alias for Options
// etc.
```


## Instalation ##

To your composer.json file add

```
#!json

"require": {
    "komanc/phinxhelper": "dev-master"
},

"repositories": [{
    "type": "vcs",
    "url": "https://bitbucket.org/Komanc/phinxhelper.git"
}]
```

## Generate config file - phinx.php ##

You can use `ConfigGenerator()` class to generate phinx config array.

### Examples of phinx.php ###

```
#!php

$options = array(
    'database' => array(
        'dsn' => 'mysql:host=127.0.0.1;dbname=test;',
        'username' => 'test', 
        'password' => '',
    ),
    'migrations_path' => '../../migrations',
);

$phinxConfig = new \PhinxHelper\Helpers\ConfigGenerator($options);
return $phinxConfig->getConfig();
```

Or fill `$options` array with data from your `config.neon`

```
#!php

$config = new Nette\DI\Config\Adapters\NeonAdapter;
$data = $config->load($file);

$options = array(
    'database' => $data['database'],
    'migrations_path' => '../../migrations',
);

$phinxConfig = new \PhinxHelper\Helpers\ConfigGenerator($options);
return $phinxConfig->getConfig();
```

Option `$options['migrations_path']` is relative path to your `phinx.php` file.

## Create migration ##


```
#!sh

$ vendor/bin/phinx create -t vendor/komanc/phinxhelper/src/PhinxHelper/HelperMigration.template.php.dist <MigrationName>
```